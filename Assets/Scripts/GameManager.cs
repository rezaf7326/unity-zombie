﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public static GameManager shared = null;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject game_over;
    [SerializeField] private AudioClip sfxMenu;
    [SerializeField] private AudioClip sfxMain;
    private AudioSource audioSource;
    private bool playerActive = false;
    private bool gameOver = false;
    private bool gameStartedOnMenu = false;

    public bool isPlayerActive{
       get { return playerActive; }
    }
    public bool isGameOver{
       get { return gameOver; }
    }
    public bool gameStartMenuPressed {
        get {return gameStartedOnMenu; }
    }

    void Awake() {
        if (shared == null) {
            shared = this;
        }else if (shared != this) {
            Destroy (gameObject);
        }
        //DontDestroyOnLoad (gameObject);

        Assert.IsNotNull (mainMenu);
        game_over.SetActive(false);
    }

    void Start() {
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
        audioSource.clip = sfxMenu;
        audioSource.Play();
    }

    public void playerStartedTheGame () {
        playerActive = true;
    }

    public void playerCollided () {
        gameOver = true;
    }

    public void EnterGame () {
        mainMenu.SetActive(false);
        gameStartedOnMenu = true;
        audioSource.Stop();
        audioSource.clip = sfxMain;
        audioSource.Play();
    }

    public void gameFinish () {
        audioSource.Stop();
        audioSource.clip = sfxMenu;
        audioSource.Play();
        game_over.SetActive(true);
        Invoke("restartGame", 5);
    }

    private void restartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
