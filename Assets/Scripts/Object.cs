﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour {
    [SerializeField] private float ObjectSpeed = 5;

    // Update is called once per frame
    void Update() {
        if (!GameManager.shared.isGameOver && GameManager.shared.isPlayerActive) {
            transform.Translate(Vector3.left * (ObjectSpeed * Time.deltaTime));
            if (transform.localPosition.z != -23.41484f || transform.localPosition.y != -15.92752f) {
                Vector3 updateZY = new Vector3(transform.position.x, -15.92752f, -23.41484f); 
                transform.position = updateZY;           
            }
            if (transform.localPosition.x <= -112.5853f) {
                Vector3 resetPos = new Vector3(62.2569f, transform.position.y, transform.position.z);
                transform.position = resetPos;
            }
        }
    }
}
