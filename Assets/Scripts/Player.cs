﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Player : MonoBehaviour
{
    [SerializeField] private float jumpForce = 100f;
    [SerializeField] private AudioClip sfxJump;
    [SerializeField] private AudioClip sfxDeath;

    private Animator anim;
    private Rigidbody rigid;
    private bool jump = false;
    private AudioSource audioSuorce;
    
    void Awake() {
        Assert.IsNotNull (sfxDeath);
        Assert.IsNotNull (sfxJump);

    }
    void Start() {
        anim = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
        audioSuorce = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.shared.isGameOver && GameManager.shared.gameStartMenuPressed) {
            if (Input.GetMouseButtonDown (0)) {
                if (GameManager.shared.isPlayerActive) { 
                    anim.Play("jump");
                    audioSuorce.PlayOneShot(sfxJump);
                    rigid.useGravity = true;
                    jump = true;
                }else {
                    GameManager.shared.playerStartedTheGame();
                }
            }
        }    
        if (transform.position.y <= -2.2) {
            float lowest = -2.2f;
            Vector3 keepLeastHeight = new Vector3 (transform.position.x, lowest, transform.position.z);
            transform.position = keepLeastHeight;
        }
    }
    
    void FixedUpdate() {
        if (jump == true) {
            rigid.velocity = new Vector2(0, 0);
            rigid.AddForce(new Vector2 (0, jumpForce), ForceMode.Impulse);
            jump = false;
        }
    }

    void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "rock") {
                audioSuorce.PlayOneShot(sfxDeath);
                rigid.AddForce(new Vector2 (-50, 20), ForceMode.Impulse);
                rigid.detectCollisions = false;
                GameManager.shared.playerCollided();
                GameManager.shared.gameFinish();
            }
    }
}
