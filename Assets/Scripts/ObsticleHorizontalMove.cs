﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsticleHorizontalMove : MonoBehaviour
{
    [SerializeField] private float objSpeed = 1;
    [SerializeField] private float y_initialPos = 1;

    [SerializeField] private float resetPosition = 1;
    [SerializeField] private float startPosition = 1;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.shared.isGameOver && GameManager.shared.isPlayerActive) { 
            transform.Translate(Vector3.left * (objSpeed * Time.deltaTime));
            if (transform.localPosition.x <= resetPosition) {
                Vector3 newPos = new Vector3(startPosition, transform.position.y, transform.position.z);
                transform.position = newPos;
                y_initialPos = Random.Range(-2.2f, 4.69f);
                transform.position = new Vector3(transform.position.x, y_initialPos, transform.position.z);
            }
        }    
    }
}
